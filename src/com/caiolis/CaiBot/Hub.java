package com.caiolis.CaiBot;

import java.io.*;
import java.util.Scanner;
import java.net.*;

public class Hub {
	
	private static final String IRCServer = "irc.freenode.org";
	private static final int IRCPort = 6667;
	private static final String channel = "#shellcity";
	private static final String nick = "CaiBot";
	private static final String user = "caiolis 8 * :caiolis";
	private static final String versionNumber = "1.0";
	
	private BufferedReader input;
	private BufferedWriter output;
	private Scanner scanner = new Scanner(System.in);
	private Socket socket;
	private boolean shuttingDown = false;
	private long pingStartTime = -1;
	
	public static void main(String[] args) {
		(new Hub()).begin();
	} // close main
	
	private void begin() {
		try {
			socket = new Socket(IRCServer, IRCPort);
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			login();
			(new Thread(new ServerMonitor())).start();
			(new Thread(new TextMonitor())).start();
		} catch (IOException e) {
			errorHandling("Something screwed up while creating a connection with the server :/", e);
		}
	} // close begin
	
	private void login() throws IOException {
		System.out.println("\t\t[[[HI I'M CAIBOT! v" + versionNumber + "]]]");
		System.out.print("Password for NickServ login with " + nick + ": ");
		String password = scanner.nextLine();
		output.write("USER " + user + "\r\n");
		output.write("NICK " + nick + "\r\n");
		output.write("PRIVMSG NickServ :IDENTIFY " + password + "\r\n");
		output.write("JOIN " + channel + "\r\n");
		output.flush();
	} // close login
	
	private class TextMonitor implements Runnable {
		
		public void run() {
			Thread.currentThread().setName("Text Monitor");
			String message;
			String[] tempArray;
			try {
				while ((message = scanner.nextLine()) != null) {
					if (message.length() > 0 && message.charAt(0) == '/') {
						if (message.substring(1).trim().equalsIgnoreCase("quit")) {
							output.write("QUIT\r\n");
							output.flush();
							shuttingDown = true;
							socket.close();
							System.exit(0);
						} else if ((tempArray = message.split(":")).length > 1 && tempArray[0].equalsIgnoreCase("/ping ") && !tempArray[1].equals("")){
							message = message.substring(1) + "\r\n";
							(new Thread(new PingChecker())).start();
							pingStartTime = System.currentTimeMillis();
						} else
							message = message.substring(1) + "\r\n";
					} else
						message = "PRIVMSG " + channel + " :" + message + "\r\n";
					output.write(message);
					output.flush();
				}
			} catch (IOException e) {
				errorHandling("Uh oh, there was a problem sending the message :/", e);
			}
			scanner.close();
		} // close run
		
	} // close TextMonitor
	
	
	private class ServerMonitor implements Runnable {
		
		public void run() {
			Thread.currentThread().setName("Server Monitor");
			String message;
			try {
				while ((message = input.readLine()) != null) {
					String[] pieces = message.split(":");
					if (pieces[0].contains("PING"))
						// Ping is in the format --->>>PING :<server's domain>
						pong(pieces[1], output);
					else if (pieces[1].contains("JOIN"))
						// Join is in the format --->>>:<nick>!<user's domain> JOIN #<channel>
						greet(pieces[1]);
					else if (pieces[1].contains("PRIVMSG"))
						// Note that the substring gets the beginning of the sender's message
						chat(pieces[1], message.substring(message.substring(1).indexOf(':') + 2));
					else if (pieces[1].contains("PART") && (pieces.length > 2 ? !pieces[2].contains("Not enough parameters") : true))
						// the complex logic is used to make sure that I don't type /part and crash the bot
						farewell(pieces[1]);
					else if (pieces[1].contains("KICK"))
						kick(message);
					else if (pieces[1].contains("#"))
						System.out.println("[SERVER] : " + message.substring(message.indexOf('#')));
					else if (pieces[1].contains("QUIT"))
						quit(message);
					else
						System.out.println("[SERVER] : " + message.substring(message.substring(1).indexOf(':') + 2));
					
					if (pingStartTime != -1)
						if (pieces[1].contains("PONG"))
							pingTime();
				}
			} catch (IOException e) {
				errorHandling("Uh oh, something happened while reading from the server :/", e);
			}
		} // close run
		
		private void pingTime() {
			long pingEndTime = System.currentTimeMillis();
			int timeElapsed = (int)(pingEndTime - pingStartTime);
			System.out.println("*** PING TIME: " + timeElapsed + " ms ***");
			pingStartTime = -1;
		} // close pingTime
		
		private void pong(String message, BufferedWriter output) {
			String pong = "PONG :" + message + "\r\n";
			try {
				output.write(pong);
				output.flush();
			} catch (IOException e) {
				errorHandling("Failed to pong :/", e);
			}
		} // close pong
		
		private void greet(String message) {
			String nick = message.split("!")[0];
			String channel = message.split("#")[1].trim();
			System.out.println("*** " + nick + " has joined " + channel + " ***");
			if (channel.equals(Hub.channel.substring(1)) && !nick.equals(Hub.nick)) {
				try {
					output.write("PRIVMSG " + Hub.channel + " :Please welcome " + nick + " to " + Hub.channel + "!!!\r\n");
					output.flush();
				} catch (IOException e) {
					errorHandling("Problem greeting a newcomer :/", e);
				}
			}
		} // close greet
		
		private void chat(String nickBlock, String message) {
			String nick = nickBlock.split("!")[0];
			String sendForm = ("[" + nick + (nickBlock.contains(Hub.channel) ? "" : " -> CaiBot") + "] : ");
			System.out.println(sendForm + message);
		} // close chat
		
		private void farewell(String message) {
			String nick = message.split("!")[0];
			String channel = message.split("#")[1].trim();
			System.out.println("*** " + nick + " has left " + channel + " ***");
			if (channel.equals(Hub.channel.substring(1))) {
				try {
					output.write("PRIVMSG " + Hub.channel + " :Who really needed " + nick + " anyway?\r\n");
					output.flush();
				} catch (IOException e) {
					errorHandling("Problem saying goodbye :/", e);
				}
			}
		} // close farewell
		
		private void kick(String message) {
			String kickInfo = message.substring(message.indexOf('#'));
			int indexOfSpace = kickInfo.indexOf(' ');
			int indexOfColon = kickInfo.indexOf(':');
			String kickedNick = kickInfo.substring(indexOfSpace + 1, indexOfColon - 1);
			String kickedFrom = kickInfo.substring(0, indexOfSpace);
			String kickMessage = kickInfo.substring(indexOfColon + 1);
			message = "*** " + kickedNick + " was kicked from " + kickedFrom + "(\"" + kickMessage + "\") ***";
			System.out.println(message);
		} // close kick
		
		private void quit(String message) {
			String nick = message.substring(1, message.indexOf('!'));
			String partingMessage = message.substring(message.substring(1).indexOf(':') + 1);
			System.out.println("*** " + nick + " has quit (\"" + partingMessage + "\") ***");
		} // close quit
		
	} // close ServerMonitor
	
	private class PingChecker implements Runnable {

		public void run() {
			Thread.currentThread().setName("Ping Checker");
			while (pingStartTime != -1) {
				if (System.currentTimeMillis() - pingStartTime > 10000) {
					System.out.println("*** PING TIMEOUT: ping exceeded 10 seconds ***");
					pingStartTime = -1;
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		} // close run
		
	} // close PingChecker
	
	private void errorHandling(String errorMessage, IOException e) {
		if (!shuttingDown) {
			System.out.println(errorMessage);
			e.printStackTrace();
		}
	} // close errorHandling
	
} // close Hub
