This is an IRC bot which runs on a hardcoded channel because of its 
certain greeting features and such which would just be impolite to have 
on any other channel. It can execute any command as well as chat. It 
greets and farewells any newcomers on a specified, hardcoded channel. 
Updates are coming!

Directions:
The bot runs from the command line. Without one open, you won't be 
seeing much.
To send a message to the channel:
	Merely type what you want and press enter.
To execute a command:
	Type a '/' character preceding your command. Note that this bot 
	is not yet very format friendly, unlike your favorite IRC 
	client, so you will need to check on the specific forms of the 
	IRC commands if the feature will be of any use to you.

This is an ongoing project, so there is certainly more to come.